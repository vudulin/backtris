import express, { json } from 'express';
import multer, { diskStorage } from 'multer';
import cors from 'cors';
import mongoose from 'mongoose';

import { registerValidation, loginValidation } from './validations.js';
import { checkAuth, handleValidationErrors } from './utils/index.js';
import { register, login, getMe, userNameCheck } from './controllers/UserController.js';

import { compare } from 'bcrypt';

mongoose
  .connect("mongodb+srv://paperonixx:paperonixx859931@cluster00.ud3xprw.mongodb.net/Tetris?retryWrites=true&w=majority")
  .then(() => console.log('DB ok'))
  .catch((err) => console.log('DB error', err));

const app = express();
app.use(json());

const storage = diskStorage({
    destination: (_, __, cb) => {
        cb(null, 'uploads');
    },
    filename: (_, file, cb) => {
        cb(null, file.originalname);
    },
});

const upload = multer({ storage });

const start = () => {
    try {
        app.listen(4444, (err) => {console.log('Server OK')});
    } catch(e) {
        console.log(e);
    }
}

app.use(cors());
app.use('/uploads', express.static('uploads'));

app.get('/auth/check/:username', userNameCheck);
app.post('/auth/login', loginValidation, handleValidationErrors, login);
app.post('/auth/register', registerValidation, handleValidationErrors, register);
app.get('/auth/me', checkAuth, getMe);

app.post('/uploads', checkAuth, upload.single('image'), (req, res) => {
    res.json({
        url: `/uploads/${req.file.originalname}`,
    });
});

start();