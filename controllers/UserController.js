import jwt from 'jsonwebtoken';
import { genSalt, hash, compare } from 'bcrypt';
import UserModel from '../models/User.js';

export const userNameCheck = async (req, res) => {
    const user = await UserModel.findOne({userName: {$eq: req.params.username} });

    if (user) { 
        return res.json({
            message: "Пользователь с таким именем уже существует",
        });
    }
    res.json({
        message: "ok"
    });
}

export const register = async (req, res) => {
    try {
        let password = req.body.password;
        let salt = await genSalt(10);
        let passwordHash = await hash(password, salt);

        let doc = new UserModel({
            email: req.body.email,
            userName: req.body.userName,
            avatarUrl: req.body.avatarUrl,
            passwordHash: passwordHash
        });

        let user = await doc.save();

        let token = jwt.sign({
            id: user.id,
        }, 'secret123', {expiresIn: '30d'});

        let userData = {
            userName: user.userName,
            email: user.email,
            avatarUrl: user.avatarUrl,
            id: user.id,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt,
        }

        res.json({userData, token})
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Не удалось зарегестрироваться',
        });
    }
}

export const login = async (req, res) => {
    try {
        const user = await UserModel.findOne({email: {$eq: req.body.email}});

        if (!user) {
            return res.status(404).json({
                message: 'Пользователь не найден',
            });
        }

        let isValidPass = await compare(req.body.password, user.passwordHash);

        if(!isValidPass) {
            return res.status(400).json({
                message: 'Неверный логин или пароль',
            });
        }

        let token = jwt.sign({
            id: user.id,
        }, 'secret123', {expiresIn: '30d'});

        let userData = {
            userName: user.userName,
            email: user.email,
            avatarUrl: user.avatarUrl,
            id: user.id,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt,
        }

        res.json({userData, token})

    } catch(err) {
        console.log(err);
        res.status(500).json({
            message: 'Не удалось авторизоваться',
        });
    }
}

export const getMe = async (req, res) => {
    try {
        const user = await UserModel.findOne({id: {$eq: req.userId}});

        if(!user) {
            return res.status(404).json({
                message: 'Пользователь не найден'
            });
        }

        let userData = {
            userName: user.userName,
            email: user.email,
            avatarUrl: user.avatarUrl,
            id: user.id,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt,
        }

        res.json(userData)
    } catch (err) {
        console.log(err);
        res.status(500).json({
            message: 'Нет доступа',
        });
    }
}