import { body } from 'express-validator';

const registerValidation = [
    body('email', 'Неверный формат почты').isEmail(),
    body('password', 'Пароль должен состоять как минимум из 8 символов').isLength({ min: 8 }),
    body('userName', 'Укажите имя').isLength({ min: 3 }),
    body('avatarUrl', 'Неверная ссылка на аватарку').optional().isURL(),
];

const loginValidation = [
    body('email', 'Неверный формат почты').isEmail(),
    body('password', 'Пароль должен состоять как минимум из 8 символов').isLength({ min: 8 }),
];


export {
    registerValidation,
    loginValidation
}